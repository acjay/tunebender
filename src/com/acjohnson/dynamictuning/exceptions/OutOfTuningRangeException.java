package com.acjohnson.dynamictuning.exceptions;

/**
 * 
 * @author Alan C. Johnson
 *
 */
public class OutOfTuningRangeException extends TuningException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
