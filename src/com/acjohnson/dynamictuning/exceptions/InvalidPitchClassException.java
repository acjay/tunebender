package com.acjohnson.dynamictuning.exceptions;

public class InvalidPitchClassException extends TuningException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String invalidPC;
	
	public InvalidPitchClassException(String s)
	{
		invalidPC = s;
	}
}