/**
 * 
 */
package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;

/**
 * @author Alan Johnson
 * @date Oct 18, 2010
 * 
 *       This interface exists to differentiate actual diatonic tunings from virtual diatonics tunings, i.e. diatonic tuning
 *       bends. Although bends act like actual tunings in many respects, in other respects they do not. They do not have a
 *       true base pitch class or base frequency, for example. This means they cannot be used as base for a tuning pivot, the
 *       way the library currently works. This may be changed in the future.
 */

public interface TrueDiatonicTuning extends VirtualDiatonicTuning
{
	/**
	 * This method must be called before using a DiatonicTuning object in order
	 * to set the base pitch. The base frequency should be set as well.
	 * 
	 * @param pitchClassName
	 * @throws InvalidPitchClassException
	 */
	public void setBasePitchClass(String pitchClassName)
			throws InvalidPitchClassException;
	
	/**
	 * 
	 * @return the base pitch class
	 */
	public String getBasePitchClass();
	
	
}
