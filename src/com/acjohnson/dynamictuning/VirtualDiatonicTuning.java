/**
 * 
 */
package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.TuningException;

/**
 * @author Alan Johnson
 * @date Oct 18, 2010 a not
 * 
 *       This interface defines methods that should be implemented by diatonic
 *       tunings: those that maintain a correspondence with standard diatonic
 *       scale.
 * 
 *       The String pitchClassName represents a pitch class, which can be "A",
 *       "B", "C", "D", "E", "F", or "G", followed by any number of "b" or "#"
 *       characters, which represent flats and sharps, respectively. Internally,
 *       conflicting accidentals will be canceled, but all other accidentals
 *       will be preserved. Pitch classes will not be converted to enharmonic
 *       equivalents, because inharmonicity is tuning-specific.
 */
public interface VirtualDiatonicTuning extends Tuning
{

	/**
	 * This method sets the pitch class on which each new octave starts. By
	 * default, this is presumed to be the base pitch of the tuning.
	 * 
	 * @param pitchClassName
	 * @throws InvalidPitchClassException
	 */
	public void setOctaveStartPitchClass(String pitchClassName)
			throws InvalidPitchClassException;
	
	/**
	 * 
	 * @return the pitch class on which each new octave starts
	 */
	public String getOctaveStartPitchClass();

	/**
	 * This method returns the frequency in the current tuning that corresponds
	 * to pitchClassName in the given octave. The base pitch for the tuning is
	 * the set to be the first pitch in octave 0. So if the tuning is based on a
	 * 660Hz E and the pitches increase in frequency, wrapping around to Ab and
	 * up to Eb.
	 * 
	 * @param pitchClassName
	 *            a note name, which can be "A", "B", "C", "D", "E", "F", or
	 *            "G", followed by any number of "b" or "#" characters, which
	 *            represent flats and sharps, respectively
	 * @param octave
	 *            the octave number with reference to the base pitch
	 * @return the frequency of the chosen pitch
	 * @throws TuningException
	 *             if pitchClassName is not correctly formatted
	 */
	double pitchToFrequency(String pitchClassName, int octave)
			throws TuningException;

}
