/**
 * 
 */
package com.acjohnson.dynamictuning;

/**
 * @author Alan C. Johnson
 *
 * This class can represent any abstract equal temperament.
 */
public class GenericEqualTemperament implements OctaveDivisionTuning
{
	private double baseFrequency;
	private double octaveRatio;
	private int octaveDivisions;
	
	public GenericEqualTemperament()
	{
		baseFrequency = 440;
		octaveRatio = 2;
		octaveDivisions = 12;
	}
	
	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.Tuning#setBaseFrequency(double)
	 */
	public void setBaseFrequency(double frequency)
	{
		baseFrequency = frequency;

	}
	
	public void setOctaveRatio(double ratio)
	{
		octaveRatio = ratio;
	}
	
	public void setOctaveDivisions(int divisions)
	{
		octaveDivisions = divisions;
	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.Tuning#setTuningOption(com.acjohnson.dynamictuning.TuningOption)
	 */
	public void setTuningOption(TuningOption option)
	{

	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.EqualTemperamentTuning#oPcCToFrequency(int, int)
	 */
	public double oPcToFrequency(int octave, int pitchClass)
	{
		return oPcToFrequency(octave, pitchClass, 0);
	}	
	
	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.EqualTemperamentTuning#oPcCToFrequency(int, int, double)
	 */
	public double oPcToFrequency(int octave, int pitchClass, double cents)
	{
		return baseFrequency * Math.pow(octaveRatio, (pitchClass * 100 + cents) / (octaveDivisions * 100));
	}

	public boolean isInitialized()
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.Tuning#getBaseFrequency()
	 */
	public double getBaseFrequency()
	{
		return baseFrequency;
	}

}
