package com.acjohnson.dynamictuning;

/**
 * 
 * @author Alan C. Johnson
 * 
 *         This interface is implemented by any equal temperament or table
 *         lookup tuning. Unlike DiatonicTuning, classes that extend this
 *         interface don't deal with diatonic pitch names and intervals, they
 *         deal with pitches in octave/pitch class/cents format, which makes it
 *         considerably more flexible. For equal temperaments that work well
 *         diatonically, there are classes that implement this interface and
 *         DiatonicTuning.
 */

public interface OctaveDivisionTuning extends Tuning
{
	double oPcToFrequency(int octave, int pitchClass);
	double oPcToFrequency(int octave, int pitchClass, double cents);
}
