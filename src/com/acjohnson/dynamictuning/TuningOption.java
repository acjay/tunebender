/**
 * 
 */
package com.acjohnson.dynamictuning;

/**
 * @author Alan Johnson
 * @date Oct 19, 2010
 * 
 *       This interface is used by individual Tuning subclasses to define any
 *       options they might respond to
 */
public interface TuningOption
{

}
