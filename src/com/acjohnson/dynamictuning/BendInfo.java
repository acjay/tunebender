/**
 * 
 */
package com.acjohnson.dynamictuning;

import java.util.ArrayList;

/**
 * @author Alan C. Johnson
 * 
 */
public class BendInfo
{
	private double startPoint;
	private ArrayList<Double> durations = new ArrayList<Double>();
	private ArrayList<Double> nextPoints = new ArrayList<Double>();

	/**
	 * @param startPoint2
	 * @param durations2
	 * @param nextPoints2
	 */
	public BendInfo(double startPoint, ArrayList<Double> durations, ArrayList<Double> nextPoints)
	{
		this.startPoint = startPoint;
		this.durations = durations;
		this.nextPoints = nextPoints;
	}

	/**
	 * @return the startPoint
	 */
	public double getStartPoint()
	{
		return startPoint;
	}

	/**
	 * @param startPoint
	 *            the startPoint to set
	 */
	public void setStartPoint(double startPoint)
	{
		this.startPoint = startPoint;
	}

	/**
	 * Add a new linear segment to the bend schedule
	 * 
	 * @param duration
	 *            the amount of time to take to get to the next point
	 * @param nextPoint
	 *            the next point
	 */
	public void addLinearSegment(double duration, double nextPoint)
	{
		durations.add(duration);
		nextPoints.add(nextPoint);
	}

	/**
	 * Calculates the value of the bend evelope at the given time offset from the start time of the bend
	 * 
	 * @param offset
	 *            the time that has elapsed since the start of the bend
	 * @return the bend index at the given offset
	 */
	public double getBendIndex(double offset)
	{
		// If for some reason the given offet is negative, return the start point
		if (offset < 0) return startPoint;

		double fromPoint = startPoint;
		double fromTime = 0;

		// Figure out which two points in the list the offset lies between
		for (int i = 0; i < durations.size(); i++)
		{
			double toPoint = nextPoints.get(i);
			double duration = durations.get(i);
			double toTime = fromTime + duration;

			// If the offset is greater than the time of the next point, move on
			if (offset > toTime)
			{
				fromPoint = toPoint;
				fromTime = toTime;
				continue;
			}

			// Return the interpolated value between the two nearest points
			return fromPoint + (toPoint - fromPoint) * ((offset - fromTime) / (toTime - fromTime));
		}

		// If the offset is past the end of the list of segments, just return the final point
		return nextPoints.get(nextPoints.size() - 1);
	}

	/**
	 * Calculates the total length of the bend in time
	 * 
	 * @return the length of the bend in time
	 */
	public double getBendLength()
	{
		double totalLength = 0;
		for (double duration : durations)
		{
			totalLength += duration;
		}
		return totalLength;
	}
}
