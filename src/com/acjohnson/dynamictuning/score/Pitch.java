package com.acjohnson.dynamictuning.score;

public class Pitch
{
	public String pitchClass;
	public int octave;
	
	public Pitch(String pitchClass, int octave)
	{
		this.pitchClass = pitchClass;
		this.octave = octave;
	}
}