/**
 * 
 */
package com.acjohnson.dynamictuning.score;

/**
 * @author Alan C. Johnson
 *
 */
public class PivotDiatonicTuningEvent extends TuningChangeEvent
{
	public String tuningName;
	public String basePitchClassName;
	public String pivotPitchClassName;
}
