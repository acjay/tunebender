/**
 * 
 */
package com.acjohnson.dynamictuning.score;

/**
 * @author Alan C. Johnson
 *
 */
public class SetOctaveStartPitchEvent implements ScoreEvent
{
	public String pitch;
}
