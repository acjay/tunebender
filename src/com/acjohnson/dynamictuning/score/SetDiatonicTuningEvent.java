package com.acjohnson.dynamictuning.score;

public class SetDiatonicTuningEvent extends TuningChangeEvent
{
	public String tuningName;
	public String basePitchClassName;
	public double baseFrequency;
}
