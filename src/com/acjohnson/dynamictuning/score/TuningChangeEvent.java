/**
 * 
 */
package com.acjohnson.dynamictuning.score;

import java.util.ArrayList;

/**
 * @author Alan C. Johnson
 *
 * This is the superclass for set and pivot tuning events
 */
public class TuningChangeEvent implements ScoreEvent
{
	public boolean doBend = false;
	public double startPoint;
	public ArrayList<Double> durations;
	public ArrayList<Double> nextPoints; 
}
