package com.acjohnson.dynamictuning.score;

import java.util.ArrayList;

public class NoteEvent implements ScoreEvent
{
	public double duration;
	public double holdTime;
	public double amplitude;
	public boolean debug;
	public ArrayList<Pitch> pitchList;
	
	public String toString()
	{
		String toReturn = "D:" + duration + " H:" + holdTime + " A:" + amplitude;
		for (Pitch pitch : pitchList)
		{
			toReturn += " " + pitch.pitchClass + pitch.octave;
		}
		return toReturn;
	}
}
