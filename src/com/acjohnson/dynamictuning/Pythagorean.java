/**
 * 
 */
package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.TuningException;

/**
 * @author Alan C. Johnson
 *
 */
public class Pythagorean extends AbstractDiatonicTuning
{

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.AbstractDiatonicTuning#setTuningOption(com.acjohnson.dynamictuning.TuningOption)
	 */
	@Override
	public void setTuningOption(TuningOption option)
	{

	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.AbstractDiatonicTuning#pitchToFrequency(java.lang.String, int)
	 */
	@Override
	public double pitchToFrequency(String pitchClass, int octave)
			throws TuningException
	{
		int octaveStartPCFifthsDistance = CircleOfFifths.findDistance(basePC, octaveStartPC);
		
		PitchClass targetPC = PitchClass.getPitchClass(pitchClass);
		int targetPCFifthsDistance = CircleOfFifths.findDistance(basePC, targetPC);
		
		double octaveStartFrequency = tuneByFifths(octaveStartPCFifthsDistance);
		double targetFrequency = tuneByFifths(targetPCFifthsDistance);
		
		if (octaveStartFrequency > baseFrequency) octaveStartFrequency /= 2;
		
		if (targetFrequency >= octaveStartFrequency * 2) targetFrequency /= 2;
		
		targetFrequency *= Math.pow(2, octave);
		
		return targetFrequency;
	}

	/**
	 * @param targetPCFifthsDistance
	 * @return
	 */
	private double tuneByFifths(int targetPCFifthsDistance)
	{
		double outputFrequency = baseFrequency;
		
		for (int i = 0; i < targetPCFifthsDistance; i++)
		{
			outputFrequency *= 1.5;
			
			//If we have moved into the next octave, normalize back down into the base octave
			if (outputFrequency > 2.0 * baseFrequency) outputFrequency /= 2.0; 
		}
		
		for (int i = 0; i > targetPCFifthsDistance; i--)
		{
			outputFrequency /= 1.5;
			
			//If we have moved into the next lower octave, normalize back up into the base octave
			if (outputFrequency <  baseFrequency) outputFrequency *= 2.0; 
		}
		
		return outputFrequency;
	}
	

}
