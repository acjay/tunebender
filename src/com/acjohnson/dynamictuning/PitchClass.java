package com.acjohnson.dynamictuning;

import java.util.Arrays;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;

public class PitchClass
{
	PitchClass()
	{
		name = "";
		accidentals = 0;
	}
	public String name;
	public int accidentals;
	
	@Override
	public String toString()
	{
		String toReturn = name;
		
		//At most one of these loops will execute
		for (int i = 0; i < accidentals; i++) toReturn += "#";
		for (int i = 0; i > accidentals; i--) toReturn += "b";
		
		return toReturn;
		
	}
	
	protected static PitchClass getPitchClass(String pitchClassName) throws InvalidPitchClassException
	{
		PitchClass newPC = new PitchClass();
		
		String name = pitchClassName.substring(0, 1);
		if (invalidPitchClassName(name)) throw new InvalidPitchClassException(pitchClassName);
		newPC.name = name;
		
		String accidentals = pitchClassName.substring(1);
		
		while (!accidentals.isEmpty())
		{
			String currentAccidental = accidentals.substring(0, 1); 
			if (currentAccidental.equals("#"))
			{
				newPC.accidentals++;
			}
			else if (currentAccidental.equals("b"))
			{
				newPC.accidentals--;
			}
			else
			{
				throw new InvalidPitchClassException(pitchClassName);
			}
			accidentals = accidentals.substring(1);
		}
		
		return newPC;
	}
	
	private static boolean invalidPitchClassName(String name)
	{
		String[] allNames = {"A", "B", "C", "D", "E", "F", "G"};
		if (Arrays.binarySearch(allNames, name) < 0) return true;

		return false;
	}
}