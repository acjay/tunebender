/**
 * 
 */
package com.acjohnson.dynamictuning;

import java.util.HashMap;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.OutOfTuningRangeException;
import com.acjohnson.dynamictuning.exceptions.TuningException;
import com.acjohnson.dynamictuning.exceptions.TuningNotInitializedException;

/**
 * @author Alan C. Johnson
 * 
 *         This class defines just intonation tunings. Currently, it provides
 *         tunings for minor/major 2nd, minor/major 3rd, perfect/augmented 4th,
 *         dimished/perfect 5th, minor/major 6th, and minor/major 7th for 5
 *         different just intonation schemes.
 *         
 *         The ratios for the included variations were taken from
 */
public class JustIntonation extends AbstractDiatonicTuning implements
		TrueDiatonicTuning
{
	/**
	 * 
	 * @author Alan C. Johnson
	 * 
	 *         This class is used to encapsulate selections among the various
	 *         variations of just-intonation
	 */
	public static class JIVariation implements TuningOption
	{
		public enum Variation
		{
			JISymmetric1, JISymmetric2, JIStdAsymmetric, JIExtAsymmetric, JISevenLimit
		}

		public Variation variation;

		public JIVariation(Variation v)
		{
			variation = v;
		}
	}

	private boolean doReinterpretPitches = true;
	
	private JIVariation.Variation variation;

	private HashMap<Integer, Integer> pitchNameToIndex;

	private double[] currentVariation;

	private static double[] symmetric1 = { 1, 16.0 / 15.0, 9.0 / 8.0,
			6.0 / 5.0, 5.0 / 4.0, 4.0 / 3.0, 45.0 / 32.0, 64.0 / 45.0,
			3.0 / 2.0, 8.0 / 5.0, 5.0 / 3.0, 16.0 / 9.0, 15.0 / 8.0 };
	private static double[] symmetric2 = { 1, 16.0 / 15.0, 10.0 / 9.0,
			6.0 / 5.0, 5.0 / 4.0, 4.0 / 3.0, 45.0 / 32.0, 64.0 / 45.0,
			3.0 / 2.0, 8.0 / 5.0, 5.0 / 3.0, 9.0 / 5.0, 15.0 / 8.0 };
	private static double[] stdAsymmetric = { 1, 16.0 / 15.0, 9.0 / 8.0,
			6.0 / 5.0, 5.0 / 4.0, 4.0 / 3.0, 45.0 / 32.0, 64.0 / 45.0,
			3.0 / 2.0, 8.0 / 5.0, 5.0 / 3.0, 9.0 / 5.0, 15.0 / 8.0 };
	private static double[] extAsymmetric = { 1, 16.0 / 15.0, 9.0 / 8.0,
			6.0 / 5.0, 5.0 / 4.0, 4.0 / 3.0, 25.0 / 18.0, 36.0 / 25.0,
			3.0 / 2.0, 8.0 / 5.0, 5.0 / 3.0, 9.0 / 5.0, 15.0 / 8.0 };
	private static double[] sevenLimit = { 1, 16.0 / 15.0, 8.0 / 7.0,
			6.0 / 5.0, 5.0 / 4.0, 4.0 / 3.0, 7.0 / 5.0, 10.0 / 7.0, 3.0 / 2.0,
			8.0 / 5.0, 5.0 / 3.0, 7.0 / 4.0, 15.0 / 8.0 };

	public JustIntonation()
	{
		pitchNameToIndex = new HashMap<Integer, Integer>();
		pitchNameToIndex.put(-6, 6);
		pitchNameToIndex.put(-5, 1);
		pitchNameToIndex.put(-4, 9);
		pitchNameToIndex.put(-3, 3);
		pitchNameToIndex.put(-2, 11);
		pitchNameToIndex.put(-1, 5);
		pitchNameToIndex.put(0, 0);
		pitchNameToIndex.put(1, 8);
		pitchNameToIndex.put(2, 2);
		pitchNameToIndex.put(3, 10);
		pitchNameToIndex.put(4, 4);
		pitchNameToIndex.put(5, 12);
		pitchNameToIndex.put(6, 7);

		variation = JIVariation.Variation.JISymmetric1;
		currentVariation = symmetric1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.acjohnson.dynamictuning.AbstractDiatonicTuning#setTuningOption(com
	 * .acjohnson.dynamictuning.TuningOption)
	 */
	public JIVariation.Variation getVariation()
	{
		return variation;
	}

	public void setVariation(JIVariation.Variation variation)
	{
		this.variation = variation;
		switch (variation)
		{
		case JISymmetric1:
			{
				currentVariation = symmetric1;
			}
		case JISymmetric2:
			{
				currentVariation = symmetric2;
			}
		case JIStdAsymmetric:
			{
				currentVariation = stdAsymmetric;
			}
		case JIExtAsymmetric:
			{
				currentVariation = extAsymmetric;
			}
		case JISevenLimit:
			{
				currentVariation = sevenLimit;
			}
		}
	}

	@Override
	public void setTuningOption(TuningOption option)
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.acjohnson.dynamictuning.AbstractDiatonicTuning#pitchToFrequency(java
	 * .lang.String, int)
	 */
	@Override
	public double pitchToFrequency(String pitchClassName, int octave)
			throws TuningException
	{
		if (!isInitialized()) throw new TuningNotInitializedException();

		int octaveStartPitchIndex = calculatePitchIndex(octaveStartPC);
		double centerPitchFrequency = baseFrequency * currentVariation[octaveStartPitchIndex];
		if (centerPitchFrequency > baseFrequency) centerPitchFrequency /= 2;

		PitchClass pitchClass = PitchClass.getPitchClass(pitchClassName);
		int targetPitchIndex = calculatePitchIndex(pitchClass);
		double targetPitchFrequency = baseFrequency * currentVariation[targetPitchIndex];
		if (targetPitchFrequency >= centerPitchFrequency * 2) targetPitchFrequency /= 2;
		
		return targetPitchFrequency * Math.pow(2, octave);
	}

	/**
	 * @param pitchClassName
	 * @return
	 * @throws InvalidPitchClassException
	 * @throws OutOfTuningRangeException
	 */
	private int calculatePitchIndex(PitchClass pitchClass) throws InvalidPitchClassException, OutOfTuningRangeException
	{
		int distance = CircleOfFifths.findDistance(basePC, pitchClass);

		if (distance < -6 || distance > 6) 
		{
			if (!doReinterpretPitches)
			{
				throw new OutOfTuningRangeException();
			}
			else
			{
				// Reinterpret pitches enharmonically (the new pitch will not be equivalent, but at least there won't be an exception
				while (distance < -6)
				{
					distance += 12;
				}
				while (distance > 6)
				{
					distance -= 12;
				}
			}
		}

		int pitchIndex = pitchNameToIndex.get(distance);
		return pitchIndex;
	}

}
