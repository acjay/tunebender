/**
 * 
 */
package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.TuningException;

/**
 * @author Alan C. Johnson
 * 
 *         This class is used to interpolate pitches between two tunings. This can be used for many things. Its main intended
 *         use is to bend the tuning while holding a chord, but it can also be used to slide between two completely different
 *         notes or chords in different tunings. Trivially, it could be used to bend between two notes in the same tuning.
 *         Also, overbends can be used for potentially interesting tuning vibratos.
 * 
 *         setBaseFrequency has no effect, as base frequencies are set individually for each tuning.
 */
public class DiatonicBendTuning implements VirtualDiatonicTuning
{
	private TrueDiatonicTuning tuning1;
	private TrueDiatonicTuning tuning2;
	private double bendIndex;

	/**
	 * Creates this tuning bend object. If the octave start pitch of the second tuning doesn't match the first tuning, it
	 * will be changed to match
	 * 
	 * @param t1
	 *            tuning # 1
	 * @param t2
	 *            tuning # 2
	 */
	public DiatonicBendTuning(TrueDiatonicTuning t1, TrueDiatonicTuning t2)
	{
		tuning1 = t1;
		tuning2 = t2;
		try
		{
			tuning2.setOctaveStartPitchClass(tuning1.getOctaveStartPitchClass());
		}
		catch (InvalidPitchClassException e)
		{
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#setBaseFrequency(double)
	 */
	public void setBaseFrequency(double frequency)
	{
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#getBaseFrequency()
	 */
	public double getBaseFrequency()
	{
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.DiatonicTuning#setOctaveStartPitchClass(java.lang.String)
	 */
	public void setOctaveStartPitchClass(String pitchClassName) throws InvalidPitchClassException
	{
		tuning1.setOctaveStartPitchClass(pitchClassName);
		tuning2.setOctaveStartPitchClass(pitchClassName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.DiatonicTuning#getOctaveStartPitchClass()
	 */
	public String getOctaveStartPitchClass()
	{
		return tuning2.getOctaveStartPitchClass();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#setTuningOption(com.acjohnson. dynamictuning.TuningOption)
	 */
	public void setTuningOption(TuningOption option)
	{
	
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#isInitialized()
	 */
	public boolean isInitialized()
	{
		return tuning1.isInitialized() && tuning2.isInitialized();
	}

	// public double calculateFrequency(String pitchClassName, int octave,
	// double bendIndex) throws TuningException
	// {
	// return calculateFrequency(pitchClassName, octave, pitchClassName,
	// octave, bendIndex);
	// }
	//
	// public double calculateFrequency(String pitchClassName1, int octave1,
	// String pitchClassName2, int octave2, double bendIndex)
	// throws TuningException
	// {
	// if (!isInitialized()) throw new TuningNotInitializedException();
	//
	// double weight = (bendIndex + 1) / 2;
	//
	// return tuning1.pitchToFrequency(pitchClassName1, octave1) * weight
	// + tuning2.pitchToFrequency(pitchClassName2, octave2);
	// }

	public synchronized double getBendIndex()
	{
		return bendIndex;
	}

	public synchronized void setBendIndex(double bendIndex)
	{
		this.bendIndex = bendIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.DiatonicTuning#pitchToFrequency(java.lang.String, int)
	 */
	public synchronized double pitchToFrequency(String pitchClassName, int octave) throws TuningException
	{
		// Convert the bend index to a weighting between the two tunings
		double weight = 1 - (bendIndex + 1) / 2;

		return tuning1.pitchToFrequency(pitchClassName, octave) * weight + tuning2.pitchToFrequency(pitchClassName, octave)
				* (1 - weight);
	}

	public String toString()
	{
		return "diatonicBendTuning tuning1:(" + tuning1 + ") tuning2:(" + tuning2 + ")";
	}

}
