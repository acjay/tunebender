/**
 * 
 */
package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.TuningException;

/**
 * @author Alan Johnson
 * @date Oct 19, 2010
 * 
 */
public abstract class AbstractDiatonicTuning implements TrueDiatonicTuning
{
	public PitchClass a;

	PitchClass basePC;
	PitchClass octaveStartPC;
	double baseFrequency;
	private boolean basePCInitialized;
	private boolean baseFrequencyInitialized;

	public AbstractDiatonicTuning()
	{
		basePCInitialized = false;
		baseFrequencyInitialized = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#setRoot(java.lang.String)
	 */
	public void setBasePitchClass(String pitchClassName) throws InvalidPitchClassException
	{
		basePC = PitchClass.getPitchClass(pitchClassName);
		octaveStartPC = basePC;
		basePCInitialized = true;
	}

	public String getBasePitchClass()
	{
		return basePC.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#setBaseFrequency(double)
	 */
	public void setBaseFrequency(double frequency)
	{
		baseFrequency = frequency;
		baseFrequencyInitialized = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.Tuning#getBaseFrequency(double)
	 */
	public double getBaseFrequency()
	{
		return baseFrequency;
	}

	public void setOctaveStartPitchClass(String pitchClassName) throws InvalidPitchClassException
	{
		octaveStartPC = PitchClass.getPitchClass(pitchClassName);
	}

	public String getOctaveStartPitchClass()
	{
		return octaveStartPC.toString();
	}

	public boolean isInitialized()
	{
		return baseFrequencyInitialized && basePCInitialized;
	}

	public String toString()
	{
		return getClass().getSimpleName() + " basePitch:" + getBasePitchClass() + " baseFrequency:" + getBaseFrequency()
				+ " " + toStringAdditionalInfo();
	}

	/**
	 * This class's toString() method returns most normal info on behalf of subclass tunings, but if a tuning has anything
	 * else to add, it should override this method
	 * 
	 * @return
	 */
	protected String toStringAdditionalInfo()
	{
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.DiatonicTuning#setTuningOption(com.acjohnson.dynamictuning.TuningOption)
	 */
	public abstract void setTuningOption(TuningOption option);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.DiatonicTuning#pitchToFrequency(java.lang.String, int)
	 */
	public abstract double pitchToFrequency(String pitchClassName, int octave) throws TuningException;

}
