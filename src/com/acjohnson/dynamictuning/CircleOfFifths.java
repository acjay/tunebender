/**
 * 
 */
package com.acjohnson.dynamictuning;

import java.util.Scanner;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;


/**
 * @author Alan C. Johnson
 * 
 */
public class CircleOfFifths
{

	public static String[] pitchClasses = { "F", "C", "G", "D", "A", "E", "B" };

	/**
	 * Returns the number of steps it takes to get from pitchClass1 to
	 * pitchClass2 around the circle of fifths
	 * 
	 * @param pitchClass1
	 * @param pitchClass2
	 * @return
	 */
	public static int findDistance(PitchClass pitchClass1,
			PitchClass pitchClass2)
	{
		return comparePitchClasses(pitchClass1.name, pitchClass2.name) + 7
				* (pitchClass2.accidentals - pitchClass1.accidentals);
	}

	private static int comparePitchClasses(String pc1, String pc2)
	{
		int index1 = -1;
		int index2 = -1;

		for (int i = 0; i < pitchClasses.length; i++)
		{
			if (pitchClasses[i].equals(pc1)) index1 = i;
			if (pitchClasses[i].equals(pc2)) index2 = i;
		}

		return index2 - index1;
	}
	
	public static void main(String[] args) throws InvalidPitchClassException
	{
		Scanner in = new Scanner(System.in);
		
		System.out.println("Pitch1: ");
		String pcname1 = in.nextLine();

		System.out.println("Pitch2: ");
		String pcname2 = in.nextLine();
		

		PitchClass pc1 = PitchClass.getPitchClass(pcname1);
		PitchClass pc2 = PitchClass.getPitchClass(pcname2);
		
		System.out.println("Fifths distance: " + findDistance(pc1, pc2));
	}

}
