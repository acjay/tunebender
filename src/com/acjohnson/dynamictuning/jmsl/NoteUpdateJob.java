/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import com.softsynth.jmsl.MusicJob;

/**
 * @author Alan C. Johnson
 *
 */
public class NoteUpdateJob extends MusicJob
{
	TuningManager tuningManager;
	NoteManager noteManager;
	int updateCount;
	
	public NoteUpdateJob(TuningManager tuningManager, NoteManager noteManager)
	{
		this.tuningManager = tuningManager;
		this.noteManager = noteManager;
		updateCount = 0;
	}
	
	@Override
	public double repeat(double playTime) throws InterruptedException
	{
		// Tell the tuning manager to update it's bend index
		boolean doContinue = tuningManager.updateBend(playTime, updateCount++);
		
		// If a new tuning has been set, updateBend will return false, and 
		if (!doContinue)
		{
			finish();
			return playTime;
		}
		
		for (NoteJob note : noteManager.getSustainingNotes())
		{
			note.update();
		}
		
		// The time should not be advanced 
		return playTime;
	}
	
}
