package com.acjohnson.dynamictuning.jmsl;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import com.acjohnson.dynamictuning.BendInfo;
import com.acjohnson.dynamictuning.score.NoteEvent;
import com.acjohnson.dynamictuning.score.ParseException;
import com.acjohnson.dynamictuning.score.Pitch;
import com.acjohnson.dynamictuning.score.PivotDiatonicTuningEvent;
import com.acjohnson.dynamictuning.score.ScoreEvent;
import com.acjohnson.dynamictuning.score.ScoreParser;
import com.acjohnson.dynamictuning.score.SetDiatonicTuningEvent;
import com.acjohnson.dynamictuning.score.SetOctaveStartPitchEvent;
import com.softsynth.jmsl.Composable;
import com.softsynth.jmsl.EventScheduler;
import com.softsynth.jmsl.Instrument;
import com.softsynth.jmsl.JMSL;
import com.softsynth.jmsl.JMSLMixerContainer;
import com.softsynth.jmsl.MusicJob;
import com.softsynth.jmsl.ParallelCollection;
import com.softsynth.jmsl.Playable;
import com.softsynth.jmsl.SequentialCollection;
import com.softsynth.jmsl.jsyn.JSynMusicDevice;
import com.softsynth.jmsl.jsyn.SynthNoteAllPortsInstrument;

/**
 * As a quick hack, this class awkwardly implements ScoreProvider. The main method and ScoreProvider code should be
 * refactored into their own class
 * 
 * @author Alan C. Johnson
 * 
 */
public class ScorePlayer extends Frame implements ActionListener, ScoreProvider
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param args
	 * @throws ParseException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws ParseException, FileNotFoundException
	{
		String inFileName = args[0];

		ScorePlayer player = new ScorePlayer(inFileName);
		player.launchPlayer();
	}

	private JMSLMixerContainer mixer;
	private Instrument ins;
	private Button startButton;
	private Button stopButton;
	private Button reloadScoreButton;
	private MusicJob score;
	private NoteManager noteManager = new NoteManager();
	private TuningManager tuningManager = new TuningManager(noteManager);
	private String inFileName;
	private TextField fileTextField;
	private ScoreProvider scoreProvider;

	public ScorePlayer(String inFileName)
	{
		this.inFileName = inFileName;
		this.scoreProvider = this;
	}

	public ScorePlayer(ScoreProvider scoreProvider)
	{
		this.scoreProvider = scoreProvider;
	}

	public void launchPlayer() throws FileNotFoundException, ParseException
	{
		initJMSL();
		initMusicDevices();
		initMixer();
		initInstrument();
		if (scoreProvider.isReady())
		{
			initComposables();
		}
		buildGUI();
	}

	private void initJMSL()
	{
		JMSL.scheduler = new EventScheduler();
		JMSL.scheduler.start();
		JMSL.clock.setAdvance(0.1);
	}

	void initMusicDevices()
	{
		JSynMusicDevice dev = JSynMusicDevice.instance();
		dev.edit(this);
		dev.open();
	}

	void initMixer()
	{
		mixer = new JMSLMixerContainer();
		mixer.start();
	}

	void initInstrument()
	{
		ins = new SynthNoteAllPortsInstrument(16, patches.ShimmerHorn.class.getName());
		// ins = new SynthNoteAllPortsInstrument(8, com.softsynth.jsyn.circuits.FilteredSawtoothBL.class.getName());
		mixer.addInstrument(ins);
	}

	private void initComposables() throws ParseException, FileNotFoundException
	{
		Reader scoreReader = scoreProvider.getScoreReader();
		ScoreParser parser = new ScoreParser(scoreReader);
		parser.top();

		score = new SequentialCollection();
		int eventNumber = 0;
		for (ScoreEvent e : parser.eventList)
		{
			if (e instanceof NoteEvent)
			{
				NoteEvent n = (NoteEvent) e;
				ParallelCollection chord = new ParallelCollection();
				for (Pitch p : n.pitchList)
				{
					NoteJob note = null;
					note = new NoteJob(noteManager, tuningManager, n.duration, n.holdTime, n.amplitude, p.pitchClass,
							p.octave);
					note.setInstrument(ins);
					if (n.debug) note.setDebug(true);
					chord.add(note);
				}
				score.add(chord);
				eventNumber++;
			}
			else if (e instanceof SetDiatonicTuningEvent)
			{
				SetDiatonicTuningEvent s = (SetDiatonicTuningEvent) e;

				SetTuningJob j = new SetTuningJob(tuningManager, s.tuningName, s.basePitchClassName, s.baseFrequency);
				if (s.doBend)
				{
					j.bendInfo = new BendInfo(s.startPoint, s.durations, s.nextPoints);
				}
				score.add(j);
			}
			else if (e instanceof PivotDiatonicTuningEvent)
			{
				PivotDiatonicTuningEvent p = (PivotDiatonicTuningEvent) e;

				PivotTuningJob j = new PivotTuningJob(tuningManager, p.tuningName, p.basePitchClassName,
						p.pivotPitchClassName);
				if (p.doBend)
				{
					j.bendInfo = new BendInfo(p.startPoint, p.durations, p.nextPoints);
				}
				score.add(j);
			}
			else if (e instanceof SetOctaveStartPitchEvent)
			{
				SetOctaveStartPitchEvent s = (SetOctaveStartPitchEvent) e;

				// Quick and dirty local class to handle the job
				class SetOctaveStartPitchJob extends MusicJob
				{
					public String pitch;

					public SetOctaveStartPitchJob(String pitch)
					{
						this.pitch = pitch;
					}

					public double start(double playTime)
					{
						tuningManager.setOctaveStartPitch(pitch);
						return playTime;
					}
				}
				;

				SetOctaveStartPitchJob j = new SetOctaveStartPitchJob(s.pitch);
				score.add(j);
			}

		}

		score.addStopPlayable(new ScoreStopPlayable(this));
	}

	public void buildGUI()
	{
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, mixer.getPanAmpControlPanel());
		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout());
		startButton = new Button("Click to start");
		stopButton = new Button("Click to stop");
		reloadScoreButton = new Button("Reload score");
		buttonPanel.add(startButton);
		buttonPanel.add(stopButton);
		buttonPanel.add(reloadScoreButton);
		add(BorderLayout.SOUTH, buttonPanel);
		startButton.addActionListener(this);
		stopButton.addActionListener(this);
		reloadScoreButton.addActionListener(this);
		startButton.setEnabled(scoreProvider.isReady());
		stopButton.setEnabled(false);

		Panel inputFilePanel = new Panel();
		inputFilePanel.setLayout(new FlowLayout());
		inputFilePanel.add(new Label("Input file:"));
		fileTextField = new TextField(20);
		fileTextField.setText(inFileName);
		inputFilePanel.add(fileTextField);
		add(BorderLayout.NORTH, inputFilePanel);

		getToolkit().sync();
		pack();
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		if (source == startButton)
		{
			score.launch(JMSL.now());
			startButton.setEnabled(false);
			reloadScoreButton.setEnabled(false);
			stopButton.setEnabled(true);
		}
		else if (source == stopButton)
		{
			stop();
			startButton.setEnabled(true);
			reloadScoreButton.setEnabled(true);
			stopButton.setEnabled(false);
		}
		else if (source == reloadScoreButton)
		{
			try
			{
				inFileName = fileTextField.getText();
				initComposables();
				startButton.setEnabled(true);
			}
			catch (FileNotFoundException e1)
			{
				e1.printStackTrace();
			}
			catch (ParseException e1)
			{
				System.out.println("Parse error.");
				e1.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void stop()
	{
		tuningManager.stopUpdater();
		score.finishAll();
	}

	// This class solely exists to update the UI when the score is done playing
	private class ScoreStopPlayable implements Playable
	{
		private ScorePlayer p;

		public ScoreStopPlayable(ScorePlayer p)
		{
			this.p = p;
		}

		public double play(double arg0, Composable arg1) throws InterruptedException
		{
			p.startButton.setEnabled(true);
			p.reloadScoreButton.setEnabled(true);
			p.stopButton.setEnabled(false);
			return arg0;
		}
	}

	public TuningManager getDiatonicTuningManager()
	{
		return tuningManager;
	}

	/**
	 * 
	 * @return
	 */
	public static double getUpdatePeriod()
	{
		return .1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.jmsl.ScoreProvider#getScoreStream()
	 */
	public Reader getScoreReader()
	{
		try
		{
			return new FileReader(inFileName);
		}
		catch (FileNotFoundException e)
		{
			System.out.println("File <" + inFileName + "> cannot be opened");
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.acjohnson.dynamictuning.jmsl.ScoreProvider#isReady()
	 */
	public boolean isReady()
	{
		// If ScorePlayer is serving as its own score provider, a filename was provided, so it should always be ready
		return true;
	}

}
