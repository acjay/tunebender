package com.acjohnson.dynamictuning.jmsl;

import com.acjohnson.dynamictuning.BendInfo;
import com.acjohnson.dynamictuning.DiatonicBendTuning;
import com.acjohnson.dynamictuning.DiatonicTuningFactory;
import com.acjohnson.dynamictuning.TrueDiatonicTuning;
import com.acjohnson.dynamictuning.VirtualDiatonicTuning;
import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;

public class TuningManager
{
	NoteManager noteManager;
	NoteUpdateJob updater;
	VirtualDiatonicTuning currentTuning;
	TrueDiatonicTuning lastTrueTuning;
	private BendInfo bendInfo;
	private double bendStartTime;
	private String octaveStartPitch;

	public TuningManager(NoteManager noteManager)
	{
		this.noteManager = noteManager;
	}

	/**
	 * 
	 * @return the current tuning
	 */
	public synchronized VirtualDiatonicTuning getTuning()
	{
		return currentTuning;
	}

	/**
	 * @return the last true diatonic tuning
	 */
	public TrueDiatonicTuning getLastTrueTuning()
	{
		return lastTrueTuning;
	}


	/**
	 * 
	 * @param tuningName
	 * @param basePitchClass
	 * @param baseFrequency
	 * @param bendInfo
	 * @param bendStartTime
	 */
	public synchronized void setTuning(String tuningName, String basePitchClass, double baseFrequency, BendInfo bendInfo, double bendStartTime)
	{
		TrueDiatonicTuning newTuning = DiatonicTuningFactory.createTuning(tuningName, basePitchClass, baseFrequency, octaveStartPitch);
		changeTuning(newTuning, bendInfo, bendStartTime);
	}
	
	/**
	 * 
	 * @param tuningName
	 * @param basePitchClass
	 * @param pivotPitchClass
	 * @param bendInfo
	 * @param bendStartTime
	 */
	public synchronized void pivotTuning(String tuningName, String basePitchClass, String pivotPitchClass, BendInfo bendInfo, double bendStartTime)
	{
		// Need a true tuning to pivot from
		TrueDiatonicTuning oldTuning;
		if (!(currentTuning instanceof TrueDiatonicTuning))
		{
			oldTuning = (TrueDiatonicTuning) getLastTrueTuning();
		}
		else
		{
			oldTuning = (TrueDiatonicTuning) currentTuning;
		}
		
		if (oldTuning == null)
		{
			System.out.println("Diatonic tuning pivot cannot be used without a previously set diatonic tuning");
			System.out.println("New tuning: " + tuningName);
		}
		
		TrueDiatonicTuning newTuning = DiatonicTuningFactory.pivotDiatonicTuning(oldTuning, tuningName, basePitchClass, pivotPitchClass);
		
		changeTuning(newTuning, bendInfo, bendStartTime);
	}
	
	/**
	 * This method is used for changing tuning within a composition. It can change instantaneously or over time.
	 * 
	 * Currently, a bend can only be executed when the previous tuning is not a true tuning (i.e. not a bend). If the current
	 * tuning is a bend, rather than throwing an exception, this method simply ignores bendInfo and executes an instantaneous
	 * tuning change. In principal, it could possible to to change this behavior to allow bends from bends, but this would
	 * require some changes to the tuning classes.
	 * 
	 * @param newTuning
	 *            the destination tuning to switch to
	 * @param bendInfo
	 *            null for an instantaneous tuning change, or a BendInfo structure to define the timing of the tuning bend
	 */
	private void changeTuning(TrueDiatonicTuning newTuning, BendInfo bendInfo, double bendStartTime)
	{
		
		// Maintain a reference to the non-last diatonic tuning so that the user can pivot tunings
		if (currentTuning instanceof TrueDiatonicTuning) lastTrueTuning = (TrueDiatonicTuning) currentTuning;

		// If this isn't a tuning bend, or there isn't a valid non-virtual previous tuning, then treat this as a new tuning
		if (bendInfo == null || lastTrueTuning == null)
		{
			bendInfo = null;
			currentTuning = newTuning;
			System.out.println("Setting new tuning at time" + bendStartTime + ": " + currentTuning.toString());
			return;
		}

		this.bendInfo = bendInfo;
		this.bendStartTime = bendStartTime;

		// The last true tuning becomes the start tuning of the bend, the new tuning is the end tuning, and the stored tuning
		// is the bend
		currentTuning = new DiatonicBendTuning(lastTrueTuning, newTuning);

		System.out.println("Setting new tuning: " + currentTuning.toString());

		// Start the note updater
		if (updater != null) updater.finish();

		// Set up the update job, which will periodically tell this class to update. Add 1 to the repeat count, because 1
		// repeat will be executed immediately

		double updatePeriod = ScorePlayer.getUpdatePeriod();
		updater = new NoteUpdateJob(this, noteManager);
		updater.setRepeatPause(updatePeriod);
		updater.setRepeats((int) Math.ceil(bendInfo.getBendLength() / updatePeriod) + 1);
		updater.launch(bendStartTime);
	}

	/**
	 * @param updateCount doesn't matter -- for debugging purposes only
	 * @return true if the bend should continue, false otherwise
	 */
	public synchronized boolean updateBend(double currentTime, int updateCount)
	{
		if (bendInfo == null)
		{
			return false;
		}

		DiatonicBendTuning bend = (DiatonicBendTuning) currentTuning;

		double bendTimePoint = currentTime - bendStartTime;

		// If the bend is finished, then tell the callee to quit
		if (bendTimePoint > bendInfo.getBendLength()) return false;

		double bendIndex = bendInfo.getBendIndex(bendTimePoint);
		//System.out.println("Count: " + updateCount + " Updating tuning bend index:" + bendIndex);
		bend.setBendIndex(bendIndex);

		return true;
	}

	/**
	 * Kill the bend update job
	 */
	public synchronized void stopUpdater()
	{
		bendInfo = null;
	}

	/**
	 * @param pitch
	 */
	public synchronized void setOctaveStartPitch(String pitch)
	{
		if (currentTuning != null) 
		{
			try
			{
				currentTuning.setOctaveStartPitchClass(pitch);
			}
			catch (InvalidPitchClassException e)
			{
				e.printStackTrace();
			} 
		}

		octaveStartPitch = pitch;
	}

}
