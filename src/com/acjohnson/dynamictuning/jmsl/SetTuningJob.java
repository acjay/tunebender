/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import com.acjohnson.dynamictuning.BendInfo;
import com.softsynth.jmsl.MusicJob;

/**
 * @author Alan C. Johnson
 *
 */
public class SetTuningJob extends MusicJob
{
	String tuningName, basePitchName;
	double baseFrequency;
	private TuningManager tuningManager;
	BendInfo bendInfo = null;
	
	public SetTuningJob(TuningManager tuningManager,
			String tuningName, String basePitchName, double baseFrequency)
	{
		this.tuningManager = tuningManager;
		this.tuningName = tuningName;
		this.basePitchName = basePitchName;
		this.baseFrequency = baseFrequency;
	}

	@Override
	public double start(double playTime) throws InterruptedException
	{		
		tuningManager.setTuning(tuningName, basePitchName, baseFrequency, bendInfo, playTime);
		
		return playTime;
	}
	
}
