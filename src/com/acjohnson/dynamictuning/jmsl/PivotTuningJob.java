/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import com.acjohnson.dynamictuning.BendInfo;
import com.softsynth.jmsl.MusicJob;

/**
 * @author Alan C. Johnson
 * 
 */
public class PivotTuningJob extends MusicJob
{
	String tuningName, basePitchClass, pivotPitchClass;
	TuningManager tuningManager;
	BendInfo bendInfo = null;

	/**
	 * @param tuningManager
	 * @param tuningName
	 * @param basePitchClass
	 * @param pivotPitchClass
	 */
	public PivotTuningJob(TuningManager tuningManager, String tuningName, String basePitchClass,
			String pivotPitchClass)
	{
		this.tuningManager = tuningManager;
		this.tuningName = tuningName;
		this.basePitchClass = basePitchClass;
		this.pivotPitchClass = pivotPitchClass;
	}

	@Override
	public double start(double playTime) throws InterruptedException
	{

		tuningManager.pivotTuning(tuningName, basePitchClass, pivotPitchClass, bendInfo, playTime);

		return playTime;
	}

	

}
