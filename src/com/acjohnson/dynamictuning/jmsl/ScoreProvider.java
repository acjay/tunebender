/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import java.io.Reader;

/**
 * @author Alan C. Johnson
 *
 */
public interface ScoreProvider
{
	public Reader getScoreReader();
	public boolean isReady();
}
