/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import com.softsynth.jmsl.MusicJob;
import com.softsynth.jsyn.SynthNote;

/**
 * This class simply keeps track of what notes are currently being sustained
 * 
 * @author Alan C. Johnson
 * 
 */
public class NoteManager
{
	private HashMap<SynthNote, NoteJob> sustainingNotes;

	// This class schedules the removal of a note from the sustaining notes list. It only tracks the note job, not the
	// SynthNote, because the SynthNote could be usurped by another job
	private class NoteEndNotifierJob extends MusicJob
	{
		public NoteManager noteManager;
		public NoteJob noteJob;

		public NoteEndNotifierJob(NoteManager noteManager, NoteJob noteJob)
		{
			this.noteManager = noteManager;
			this.noteJob = noteJob;
		}

		public double start(double playTime)
		{
			noteManager.remove(noteJob);
			return playTime;
		}
	}

	public NoteManager()
	{
		sustainingNotes = new HashMap<SynthNote, NoteJob>();
	}

	public void add(NoteJob noteJob, SynthNote synthNote, double holdTime)
	{
		// If this SynthNote already has a note job attached to it, that means the polyphony max has been reached and the
		// SynthNote has been reassigned to a new note job
		if (sustainingNotes.get(synthNote) == null)
		{
			sustainingNotes.remove(noteJob);
		}
		sustainingNotes.put(synthNote, noteJob);

		MusicJob noteEndNotifier = new NoteEndNotifierJob(this, noteJob);
		noteEndNotifier.setStartDelay(holdTime);
	}

	public Collection<NoteJob> getSustainingNotes()
	{
		return sustainingNotes.values();
	}

	public SynthNote getSynthNote(NoteJob noteJob)
	{
		// There should never be more than one SynthNote associated with a DiatonicNoteJob, but if there are for some crazy
		// reason, remove all of them

		SynthNote found = null;
		for (Entry<SynthNote, NoteJob> e : sustainingNotes.entrySet())
		{
			if (e.getValue() == noteJob)
			{
				found = e.getKey();
			}
		}
		return found;
	}
	
	
	/**
	 * @param noteJob
	 */
	private void remove(NoteJob noteJob)
	{
		SynthNote synthNote = getSynthNote(noteJob);
	
		if (synthNote != null) sustainingNotes.remove(synthNote);
	
	}
}
