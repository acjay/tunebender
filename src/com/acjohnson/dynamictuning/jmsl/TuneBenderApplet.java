/**
 * 
 */
package com.acjohnson.dynamictuning.jmsl;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import com.acjohnson.dynamictuning.score.ParseException;

/**
 * @author Alan C. Johnson
 * 
 *         Does not yet work
 */
public class TuneBenderApplet extends Applet implements ActionListener, ScoreProvider
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	List lstPredefinedFiles;
	Button btnLoad;
	TextArea txaInput;

	ScorePlayer player;

	public void start()
	{
		setPreferredSize(new Dimension(400, 400));
		setLayout(new BorderLayout());

		Panel pnlPredefinedFiles = new Panel(new GridLayout(1, 3));
		pnlPredefinedFiles.add(new Label("Pre-created files:"));

		lstPredefinedFiles = new List(4, false);
		lstPredefinedFiles.add("Composition1.score");
		lstPredefinedFiles.add("Composition2.score");
		lstPredefinedFiles.add("Composition3.score");
		lstPredefinedFiles.add("Composition4.score");
		Dimension listSize = lstPredefinedFiles.getPreferredSize();
		listSize.width = 300;
		lstPredefinedFiles.setPreferredSize(listSize);
		pnlPredefinedFiles.add(lstPredefinedFiles);

		btnLoad = new Button("Load");
		btnLoad.addActionListener(this);
		pnlPredefinedFiles.add(btnLoad);

		add(BorderLayout.NORTH, pnlPredefinedFiles);

		txaInput = new TextArea(30, 80);
		add(BorderLayout.CENTER, txaInput);
		
		player = new ScorePlayer(this);
		try
		{
			player.launchPlayer();
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == btnLoad)
		{
			if (lstPredefinedFiles.getSelectedIndex() != -1)
			{	
				txaInput.setText(readTextFromJar("/" + lstPredefinedFiles.getSelectedItem()));
			}
		}

	}

	public String readTextFromJar(String s)
	{
		String fileData = "";
		String thisLine;
		try
		{
			InputStream is = getClass().getResourceAsStream(s);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while ((thisLine = br.readLine()) != null)
			{
				fileData += thisLine + "\n";
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return fileData;
	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.jmsl.ScoreProvider#getScoreStream()
	 */
	public Reader getScoreReader()
	{
		if (isReady())
		{
			return new StringReader(txaInput.getText());
		}
		else
		{
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.jmsl.ScoreProvider#isReady()
	 */
	public boolean isReady()
	{
		// If there is input, then we're ready
		return !(txaInput.getText().isEmpty());
	}

}
