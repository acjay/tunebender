 package com.acjohnson.dynamictuning.jmsl;

import com.acjohnson.dynamictuning.VirtualDiatonicTuning;
import com.acjohnson.dynamictuning.exceptions.TuningException;
import com.softsynth.jmsl.DimensionNameSpace;
import com.softsynth.jmsl.Instrument;
import com.softsynth.jmsl.MusicJob;
import com.softsynth.jmsl.jsyn.FreqSynthNoteInstrument;
import com.softsynth.jsyn.SynthException;
import com.softsynth.jsyn.SynthNote;

/**
 * 
 * @author Alan C. Johnson
 * 
 */
public class NoteJob extends MusicJob
{
	/*
	 * @Override public double stop(double playTime) throws InterruptedException { double toReturn = super.stop(playTime);
	 * 
	 * getInstrument().off(playTime, getTimeStretch(), createDar());
	 * 
	 * return toReturn; }
	 */

	double duration, holdTime, amplitude;
	String pitchClassName;
	int octave;
	TuningManager tuningManager;
	NoteManager noteManager;
	private boolean debug;

	public NoteJob(NoteManager noteManager, TuningManager tuningManager, double duration, double holdTime, double amplitude,
			String pitchClassName, int octave)
	{
		this.noteManager = noteManager;
		this.tuningManager = tuningManager;
		this.duration = duration;
		this.holdTime = holdTime;
		this.amplitude = amplitude;
		this.pitchClassName = pitchClassName;
		this.octave = octave;
	}
	
	

	/* (non-Javadoc)
	 * @see com.softsynth.jmsl.MusicJob#start(double)
	 */
	@Override
	public double start(double playTime) throws InterruptedException
	{
		return playTime;
	}


	@Override
	public double repeat(double playTime) throws InterruptedException
	{
		double updatedTime = playTime + duration * getTimeStretch();

		try
		{
			System.out.println("Playing note: " + pitchClassName + octave + "("
					+ tuningManager.getTuning().pitchToFrequency(pitchClassName, octave) + ") at time " + playTime
					+ " until " + updatedTime);
		}
		catch (TuningException e)
		{
			e.printStackTrace();
		}

		synchronized (NoteJob.class)
		{
			SynthNote synthNote = (SynthNote) getInstrument().on(playTime, getTimeStretch(), createDar());
			noteManager.add(this, synthNote, holdTime);
			((FreqSynthNoteInstrument) getInstrument()).off(playTime + holdTime, synthNote);

		}
		return updatedTime;
	}

	// @Override
	// public double play(double startTime, Composable parent)
	// throws InterruptedException
	// {
	// // The commented out code is my workaround. Below is my original attempt
	// // that hasn't been working
	//
	// double updatedTime = startTime + duration * getTimeStretch();
	// threadSafePlay((FreqSynthNoteInstrument) getInstrument(), startTime,
	// holdTime, getTimeStretch(), createDar());
	// System.out.println("Playing note: " + pitchClassName + octave
	// + " at time " + startTime + " until " + updatedTime);
	// return updatedTime;
	//
	// double updatedTime = startTime + duration * getTimeStretch();
	// mySynthNote = (SynthNote) getInstrument().on(startTime,
	// getTimeStretch(), createDar());
	// ((FreqSynthNoteInstrument) getInstrument()).off(startTime + holdTime,
	// mySynthNote);
	// System.out.println("Playing note: " + pitchClassName + octave
	// + " at time " + startTime + " until " + updatedTime);
	// return updatedTime;
	// }
	//
	// public static synchronized void threadSafePlay(FreqSynthNoteInstrument ins,
	// double startTime, double holdTime, double timeStretch, double[] dar)
	// {
	// SynthNote mySynthNote = (SynthNote) ins.on(startTime, timeStretch, dar);
	// ins.off(startTime + holdTime * timeStretch, mySynthNote);
	// }

	private double[] createDar()
	{
		Instrument ins = getInstrument();
		DimensionNameSpace dns = ins.getDimensionNameSpace();
		VirtualDiatonicTuning tuning = tuningManager.getTuning();

		double dar[] = new double[dns.dimension()];
		dar[dns.getDimension("hold")] = holdTime;
		dar[dns.getDimension("duration")] = duration;
		if (dns.getDimension("pitch") >= 0)
		{
			try
			{
				dar[dns.getDimension("pitch")] = freqToPitchNumber(tuning.pitchToFrequency(pitchClassName, octave));
			}
			catch (TuningException e)
			{
				System.err.println("Erroneous pitch");
				e.printStackTrace();
				dar[dns.getDimension("pitch")] = 0;
			}
		}
		else if (dns.getDimension("frequency") >= 0)
		{
			try
			{
				dar[dns.getDimension("frequency")] = tuning.pitchToFrequency(pitchClassName, octave);
			}
			catch (TuningException e)
			{
				System.err.println("Erroneous pitch");
				e.printStackTrace();
				dar[dns.getDimension("pitch")] = 0;
			}
		}
		dar[dns.getDimension("amplitude")] = amplitude;
		return dar;
	}

	private double freqToPitchNumber(double frequency)
	{
		return 12 * Math.log(frequency / 261.03) / Math.log(2) + 60;
	}

	/**
	 * 
	 */
	public void update()
	{
		synchronized (NoteJob.class)
		{
			SynthNote synthNote = noteManager.getSynthNote(this);
			if (synthNote != null)
			{
				try
				{
					double newFrequency = tuningManager.getTuning().pitchToFrequency(pitchClassName, octave);
					if (debug) System.out.println("New Frequency: " + newFrequency);
					synthNote.frequency.set(newFrequency);
				}
				catch (SynthException e)
				{
					e.printStackTrace();
				}
				catch (TuningException e)
				{
					e.printStackTrace();
				}	
			}
		}
	}



	/**
	 * @param b
	 */
	public void setDebug(boolean doDebug)
	{
		debug = doDebug;
	}

}
