package com.acjohnson.dynamictuning;

import com.acjohnson.dynamictuning.exceptions.TuningException;

public final class TuningTester
{
	public static void main(String[] args) throws TuningException
	{
		TrueDiatonicTuning tuning = new JustIntonation();
		tuning.setBaseFrequency(440);
		tuning.setBasePitchClass("A");
		tuning.setOctaveStartPitchClass("C");
		String[] pitches = {"A", "B", "C", "D", "E", "F", "G"};
		
		for (int i = 0; i < pitches.length; i++)
		{
			System.out.println(pitches[i] + ": " + tuning.pitchToFrequency(pitches[i], 0));
		}
	}

}
