/**
 * 
 */
package com.acjohnson.dynamictuning;

import java.util.HashMap;
import java.util.Map;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.TuningException;
import com.acjohnson.dynamictuning.exceptions.TuningNotInitializedException;

/**
 * @author Alan Johnson
 * @date Oct 19, 2010
 *
 */
public class EqualTemperament19 extends AbstractDiatonicTuning implements OctaveDivisionTuning
{
	private Map<String, Integer> pitchNameToLocation;
	
	public EqualTemperament19()
	{
		pitchNameToLocation = new HashMap<String, Integer>();
		pitchNameToLocation.put("A", 0);
		pitchNameToLocation.put("B", 3);
		pitchNameToLocation.put("C", 5);
		pitchNameToLocation.put("D", 8);
		pitchNameToLocation.put("E", 11);
		pitchNameToLocation.put("F", 13);
		pitchNameToLocation.put("G", 16);
	}
	
	public void setTuningOption(TuningOption option)
	{
				
	}

	public double pitchToFrequency(String pitchClassName, int octave)
			throws InvalidPitchClassException, TuningNotInitializedException
	{
//		if (!isInitialized()) throw new TuningNotInitializedException(); 
//		
//		int baseNum = pitchNameToLocation.get(basePC.name) + basePC.accidentals;
//		
//		PitchClass pc = PitchClass.getPitchClass(pitchClassName);
//		int pitchNum = pitchNameToLocation.get(pc.name) + pc.accidentals;
//		if (pitchNum < baseNum) pitchNum += 19;
//		
//		int interval = pitchNum + 19 * octave - baseNum;
//		
//		return baseFrequency * Math.pow(2, interval / 19.0);
		
		if (!isInitialized()) throw new TuningNotInitializedException();

		// Convert pitches to scale steps from pitch class A
		int basePitchNum = pitchNameToLocation.get(basePC.name) + basePC.accidentals;
		int octaveStartNum = pitchNameToLocation.get(octaveStartPC.name) + octaveStartPC.accidentals;
		PitchClass pc = PitchClass.getPitchClass(pitchClassName);
		int pitchNum = pitchNameToLocation.get(pc.name) + pc.accidentals;

		// We need octaveStartNum <= basePitchNum <= pitchNum, pitchNum < octaveStartNum + 12
		
		if (pitchNum < basePitchNum) pitchNum += 19;
		
		// The base pitch of the tuning should always be in octave 0, so make sure the octave start pitch falls before the
		// base pitch
		if (octaveStartNum > basePitchNum) octaveStartNum -= 19;

		// Make sure the target pitch is within 12 scale steps of the octave start pitch -- getting the octave correct is
		// handled later
		if (pitchNum >= octaveStartNum + 19) pitchNum -= 19;

		// Calculate the number of scale steps between the base pitch and the target pitch in its proper octave
		int interval = pitchNum + 19 * octave - basePitchNum;

		// Calculate the frequency difference based from the interval
		return baseFrequency * Math.pow(2, interval / 19.0);
	}

	public static void main(String[] args) throws TuningException
	{
		String basePitchClass = args[0];
		double baseFreq = Double.parseDouble(args[1]);
		String octaveStartPitchClass = args[2];
		
		TrueDiatonicTuning et19 = new EqualTemperament19();
		et19.setBaseFrequency(baseFreq);
		et19.setBasePitchClass(basePitchClass);
		et19.setOctaveStartPitchClass(octaveStartPitchClass);
		
		String[] pitches = { "A", "B", "C", "D", "E", "F", "G" };

		for (int i = 0; i < 7; i++)
		{
			System.out.println(pitches[i] + " Freq: " + et19.pitchToFrequency(pitches[i], 0));
		}
	}

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.OctaveDivisionTuning#oPcToFrequency(int, int)
	 */
	public double oPcToFrequency(int octave, int pitchClass)
	{
		return oPcToFrequency(octave, pitchClass, 0);
	}	

	/* (non-Javadoc)
	 * @see com.acjohnson.dynamictuning.OctaveDivisionTuning#oPcToFrequency(int, int, double)
	 */
	public double oPcToFrequency(int octave, int pitchClass, double cents)
	{
		return baseFrequency * Math.pow(2, (pitchClass * 100 + cents) / 1900);
	}
}
