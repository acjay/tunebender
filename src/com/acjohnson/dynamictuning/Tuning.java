package com.acjohnson.dynamictuning;

public interface Tuning
{

	void setBaseFrequency(double frequency);
	double getBaseFrequency();
	void setTuningOption(TuningOption option);
	boolean isInitialized();
}
