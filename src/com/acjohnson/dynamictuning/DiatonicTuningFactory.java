/**
 * 
 */
package com.acjohnson.dynamictuning;

import java.util.HashMap;

import com.acjohnson.dynamictuning.exceptions.InvalidPitchClassException;
import com.acjohnson.dynamictuning.exceptions.TuningException;

/**
 * @author Alan C. Johnson
 * 
 */
public class DiatonicTuningFactory
{
	@SuppressWarnings("rawtypes")
	private static HashMap<String, Class> tuningMap = null;

	@SuppressWarnings("rawtypes")
	private static void initialize()
	{

		// This is the master list of tunings that DiatonicTuningFactory can create
		Class tuningClasses[] = { EqualTemperament12.class, EqualTemperament19.class, JustIntonation.class,
				Pythagorean.class };

		tuningMap = new HashMap<String, Class>();

		for (Class c : tuningClasses)
		{
			System.out.println("Adding tuning: " + c.getSimpleName());
			tuningMap.put(c.getSimpleName(), c);
		}
	}

	/**
	 * Create a new tuning object
	 * 
	 * @param tuningName
	 *            the name of the tuning
	 * @param basePitchClassName
	 *            the base pitch class of the tuning
	 * @param baseFrequency
	 *            the base frequency of the tuning
	 * @param octaveStartPitchClass
	 *            the pitch class that starts each octave, or null to use the base pitch
	 * @return the tuning object or null, if the parameters are incorrect
	 */
	public static TrueDiatonicTuning createTuning(String tuningName, String basePitchClass, double baseFrequency,
			String octaveStartPitchClass)
	{
		TrueDiatonicTuning newTuning;
		
		// The first time the tuning factory is used, it will be initialized. Synchronize to make sure this doesn't
		// interleave.
		synchronized (DiatonicTuningFactory.class)
		{
			if (tuningMap == null) initialize();
		}

		try
		{

			newTuning = (TrueDiatonicTuning) tuningMap.get(tuningName).newInstance();

			newTuning.setBasePitchClass(basePitchClass);
			newTuning.setBaseFrequency(baseFrequency);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
		// Set the octave start pitch class, if one is provided
		if (octaveStartPitchClass != null)
		{
			try
			{
				newTuning.setOctaveStartPitchClass(octaveStartPitchClass);
			}
			catch (InvalidPitchClassException e)
			{
				return null;
			}
		}
		
		return newTuning;
	}

	/**
	 * This method can be used to create a new tuning with a pitch that coincides in frequency with the old tuning. This is
	 * useful when modulating tunings while a chord is playing. The new tuning will automatically be set to have the same
	 * octave start pitch class, so that all pitches remain in the same octaves. If desired, the octave start pitch class of
	 * the new tuning can be changed afterward.
	 * 
	 * @param oldTuning
	 *            the old DiatonicTuning
	 * @param tuningName
	 *            the name of the new tuning
	 * @param basePitchClass
	 *            the base pitch class of the new tuning
	 * @param pivotPitchClass
	 *            the pitch on which to pivot
	 * @return the new tuning
	 */
	public static TrueDiatonicTuning pivotDiatonicTuning(TrueDiatonicTuning oldTuning, String tuningName,
			String basePitchClass, String pivotPitchClass)
	{
		// Get the base frequency and octave start pitch class from the old tuning
		double oldBaseFrequency = oldTuning.getBaseFrequency();
		String oldOctaveStartPitchClass = oldTuning.getOctaveStartPitchClass();

		// Find the frequency of the pivot pitch in the old tuning
		double pivotFrequency = -1.0;
		try
		{
			pivotFrequency = oldTuning.pitchToFrequency(pivotPitchClass, 0);
		}
		catch (TuningException e)
		{
			e.printStackTrace();
			return null;
		}

		// Naively create a new tuning with the same base frequency (the pivot pitches will probably not line up)
		TrueDiatonicTuning tempTuning = DiatonicTuningFactory.createTuning(tuningName, basePitchClass, oldBaseFrequency,
				oldOctaveStartPitchClass);

		// Find the factor by which frequencies given by each tuning for pivot pitch disagree
		double testFrequency = -1;
		try
		{
			testFrequency = tempTuning.pitchToFrequency(pivotPitchClass, 0);
		}
		catch (TuningException e)
		{
			e.printStackTrace();
			return null;
		}
		double tuningCorrectionFactor = pivotFrequency / testFrequency;

		// Create the real new tuning by scaling up the base frequency by the correction factor. Now the pivot pitches do
		// line up.
		return DiatonicTuningFactory.createTuning(tuningName, basePitchClass, oldBaseFrequency * tuningCorrectionFactor,
				oldOctaveStartPitchClass);
	}
}
